import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RickandmortyService {

  private basePath = "https://rickandmortyapi.com/api/"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    parmas: new HttpParams()
  };

  constructor( private httpClient: HttpClient) { }

  public getAllEpisodes():any {
    return this.httpClient.get(`${this.basePath}episode`, this.httpOptions);
  }
}
