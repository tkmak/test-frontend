import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Rickandmorty } from '../models/rickandmorty';

import { RickandmortyService } from './rickandmorty.service';

describe('RickandmortyService', () => {
  let service: RickandmortyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
    });
    service = TestBed.inject(RickandmortyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Service: Rickandmorty', ()=>{
    let httpClientSpy: { get: jasmine.Spy}
    let rickandmortyService: RickandmortyService;

    beforeEach(()=>{
      httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
      rickandmortyService = new RickandmortyService(httpClientSpy as any);
    });

    it('should return expected rickandmorty data (HttpClient called once)', () => {
      const expectedRickandmorty: Rickandmorty =
        { info: {count: 1, pages: 1}, results: [{ id: 1, name: 'A', air_date: 'dd', url: 'dd' }, { id: 2, name: 'B', air_date: 'dd', url: 'dd'}] };
    
      httpClientSpy.get.and.returnValue(of(expectedRickandmorty));
    
      rickandmortyService.getAllEpisodes().subscribe(
        (data: any) => expect(data).toEqual(expectedRickandmorty, 'expected Rickandmorty'),
        fail
      );;
      expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    });   

  })
});
