export interface Rickandmorty {
    info: Info;
    results: Episode[];
}

export interface Info {
    count: number;
    pages: number;
}

export interface Episode {
    id: number;
    name: string;
    air_date: string;
    url: string;
}