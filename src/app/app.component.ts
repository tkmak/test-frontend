import { Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { Rickandmorty, Episode } from './models/rickandmorty';
import { RickandmortyService } from './services/rickandmorty.service';
import { ConfirmacionEliminarComponent } from './components/confirmacion-eliminar/confirmacion-eliminar.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  pageEvent: PageEvent = {
    length: 18,
    pageIndex: 0,
    pageSize: 10,
    previousPageIndex: 0
  };  
  data: Rickandmorty = {
    info: {count: 0, pages: 0}, 
    results: []
  };
  listData: Episode[] = [];
  
  private posicionH: MatSnackBarHorizontalPosition = 'center';
  private posicionV: MatSnackBarVerticalPosition = 'top';    
  private durationInSeconds = 5;

  constructor(private rickandmortyService: RickandmortyService, 
    private _snackBar: MatSnackBar,
    public dialog: MatDialog ) {
    this.getAllEpisodes();
  }

  getAllEpisodes() {
    this.rickandmortyService.getAllEpisodes()
    .subscribe(
      (data: any)=>{
        this.data = data;
        this.listData = data.results;
      }
    )
  }

  eliminar(episode: Episode){
    const dialogRef = this.dialog.open(ConfirmacionEliminarComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {        
        this.listData = this.listData.filter( item => item !== episode);
        this._snackBar.open( 'Se elimino el registro satisfactoriamente!', 'X',{
          duration: this.durationInSeconds * 1000,
          horizontalPosition: this.posicionH,
          verticalPosition: this.posicionV,
          panelClass: ['snack-bar-success']
        })
      }
    });
  }

}
