import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { RickandmortyService } from './services/rickandmorty.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        MatSnackBarModule,
        MatDialogModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [RickandmortyService]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should call confirmacion-eliminar', ()=>{
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const episode: any = {};
    app.eliminar(episode);
  })
  
  

  // describe('When eliminar() is called', ()=> {
  //   it('should call openDialog', () => {
  //     spyOn(component.dialog, 'eliminar')
  //     const fixture = TestBed.createComponent(AppComponent);
  //     const app = fixture.componentInstance;
  //     app.dialog.open;
  //   })
  // })

  // describe('When getAllEpisodes() is called', () => {
  //   it('should be get data', ()=> {
  //     spyOn(component.rickandmortyService, 'getAllEpisodes').and.returnValue(of({data: 'Episodes'}))
  //     component.getAllEpisodes();
  //     expect(component.data).toBeTruthy();
  //   })
  // })
});
