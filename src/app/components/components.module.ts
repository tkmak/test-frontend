import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmacionEliminarComponent } from './confirmacion-eliminar/confirmacion-eliminar.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [ConfirmacionEliminarComponent],
  imports: [
    CommonModule,
    MatDialogModule, MatButtonModule
  ],
  exports: [
    ConfirmacionEliminarComponent
  ]
})
export class ComponentsModule { }
